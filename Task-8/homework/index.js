/* START TASK 1: Your code goes here */
function clickCell(){
    let row=document.getElementById('mytable').rows;
    for(let i=0;i<row.length;i++){
        for(let j=0;j<row[i].cells.length;j++){
            row[i].cells[j].addEventListener('click',function (){
                row[i].cells[j].classList.add('yellow');
                if (row[i].cells[j].innerHTML.toLowerCase() === "specialcell") {
                    row[i].cells[j].classList.remove('yellow', 'blue');
                    for (let u = 0; u < row.length; u++) {
                        for (let v = 0; v < row[u].cells.length; v++) {
                            if (row[u].cells[v].classList.contains('white')) {
                                row[u].cells[v].classList.toggle('green');
                            }
                            
                        }
                    }
                }
                if (
                    i === 0 && j === 0 ||
                    i === 1 && j === 0 ||
                    i === 2 && j === 0  ) {
                    row[i].cells[j].classList.remove('yellow');
                    for (let y = 0; y < row[i].cells.length; y++) {
                        if (row[i].cells[y].classList.value !== 'white yellow') {
                            row[i].cells[y].classList.add('blue');
                        }
                    }
                }
            });
        }
    }
}
clickCell();

/* END TASK 1 */

/* START TASK 2: Your code goes here */

function validateAndSendMessage() {
    let userInput = document.getElementById('userInput').value.trim();
    let phonePattern = /^\+380\d{9}$/;
    let messageBox = document.getElementById('messageBox');
    let notification = document.getElementById('notification');

    if (phonePattern.test(userInput)) {
        messageBox.value = 'Data was successfully sent.';
        notification.textContent = '';
        messageBox.style.backgroundColor = '#4CAF50';
        messageBox.style.color = 'white';
        messageBox.style.border = '1px solid #45a049';
    } else {
        messageBox.value = 'Type number does not follow format +380********';
        notification.textContent = 'Type number does not follow format +380********';
        document.getElementById('userInput').style.border = '1px solid red';
        messageBox.style.backgroundColor = '#D9534F';
        messageBox.style.color = 'white';
        messageBox.style.border = '1px solid #d43f3a';
    }
}

document.getElementById('userInput').addEventListener('input', function () {
    document.getElementById('userInput').style.border = '1px solid black';
    document.getElementById('notification').style.display = 'none';
    document.getElementById('messageBox').style.backgroundColor = '#4CAF50';
    document.getElementById('messageBox').style.color = 'white';
    document.getElementById('messageBox').style.border = '1px solid #45a049';
});


/* END TASK 2 */

/* START TASK 3: Your code goes here */

/* END TASK 3 */
