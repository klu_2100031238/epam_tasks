// Your code goes here
// Task #1
function isEquals(a, b) {
    return a === b;
  }
  
  // Task #2
  function isBigger(a, b) {
    return a > b;
  }
  
  // Task #3
  function storeNames(...names) {
    return names;
  }
  
  // Task #4
  function getDifference(a, b) {
    const difference = a > b ? a - b : b - a;
    return difference;
  }
  
  // Task #5
  function negativeCount(numbers) {
    return numbers.filter(num => num < 0).length;
  }
  
  // Task #6
  function letterCount(str, letter) {
    return str.split(letter).length - 1;
  }
  
  // Task #7
  function countPoints(scores) {
    return scores.reduce((totalPoints, score) => {
      const [x, y] = score.split(':').map(Number);
      if (x > y) {
        return totalPoints + 3;
      } else if (x === y) {
        return totalPoints + 1;
      } else {
        return totalPoints;
      }
    }, 0);
  }
  
  // Testing and printing the output using console.log
  console.log(isEquals(3, '3')); // should print: true
  console.log(isBigger(5, -1)); // should print: true
  console.log(storeNames('Tommy Shelby', 'Ragnar Lodbrok', 'Tom Hardy'));
  // should print: ['Tommy Shelby', 'Ragnar Lodbrok', 'Tom Hardy']
  console.log(getDifference(5, 3)); // should print: 2
  console.log(getDifference(5, 8)); // should print: 3
  console.log(negativeCount([4, 3, 2, 9])); // should print: 0
  console.log(negativeCount([0, -3, 5, 7])); // should print: 1
  console.log(letterCount("Marry", "r")); // should print: 2
  console.log(letterCount("Barny", "y")); // should print: 1
  console.log(letterCount("", "z")); // should print: 0
  console.log(countPoints(['100:90', '110:98', '100:100', '95:46', '54:90', '99:44', '90:90', '111:100']));
  // should print: 17
  