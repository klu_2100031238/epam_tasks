import logo from './logo.svg';
import './App.css';
import React, { useState } from 'react';
import region from './external-service';

function App() {
  const [selectedOption, setSelectedOption] = useState(null);
  const [selectedRegion, setSelectedRegion] = useState(null);
  const [selectedLanguage, setSelectedLanguage] = useState(null);

  const handleOptionChange = (option) => {
    setSelectedOption(option);
    // Reset selectedRegion and selectedLanguage when changing the option
    setSelectedRegion(null);
    setSelectedLanguage(null);
  };

  const handleRegionChange = (region) => {
    setSelectedRegion(region);
  };

  const handleLanguageChange = (language) => {
    setSelectedLanguage(language);
  };

  const filteredCountries = selectedOption === 'region'
    ? region.getCountryListByRegion(selectedRegion)
    : region.getCountryListByLanguage(selectedLanguage);
    
  return (
    <div className="App">
       <div>
        <br></br>
        <br></br>
        <h1>Countries Explorer</h1>
          <label>
            <input
              type="radio"
              value="region"
              checked={selectedOption === 'region'}
              onChange={() => handleOptionChange('region')}
            />
            By Region
          </label>
          <br></br>
          <label>
            <input
              type="radio"
              value="language"
              checked={selectedOption === 'language'}
              onChange={() => handleOptionChange('language')}
            />
            By Language
          </label>
        </div>

      {selectedOption === 'region' && (
        <div>
          <label>Select Region:</label>
          <select value={selectedRegion || ''} onChange={(e) => handleRegionChange(e.target.value)}>
          {/*************************/}
          <option value="" disabled>Select a Region</option>
            {region.getRegionsList().map((region) => (
              <option key={region} value={region}>{region}</option>
            ))}
          </select>
        </div>
      )}

      {selectedOption === 'language' && (
        <div>
          <label>Select Language:</label>
         
          <select value={selectedLanguage || ''} onChange={(e) => handleLanguageChange(e.target.value)}>
          <option value="" disabled>Select a Language</option>
            {region.getLanguagesList().map((language) => (
              <option key={language} value={language}>{language}</option>
            ))}
          </select>
        </div>
      )}

      <br/>
      <br/>
      {selectedOption && (selectedRegion || selectedLanguage) && (
      <table align="center" border={1}>
        <thead>
          <tr>
            <th>Name</th>
            <th>Flag</th>
            <th>Region</th>
            <th>Area</th>
            <th>Capital</th>
            <th>Languages</th>
          </tr>
        </thead>
        <tbody>
          {filteredCountries.map((country, index) => (
            <tr key={index}>
              <td>{country.name}</td>
              <td><img src={country.flagURL} alt={country.name} /></td>
              <td>{country.region}</td>
              <td>{country.area}</td>
              <td>{country.capital}</td>
              <td>{Object.values(country.languages).join(', ')}</td>
            </tr>
          ))}
        </tbody>
      </table>
      )}
   
    </div>
  );
}

export default App;
