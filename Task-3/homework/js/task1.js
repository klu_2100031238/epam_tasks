// Your code goes here

// Function to prompt user until valid input is provided
function promptUntilValid(message, validationFunction, errorMessage) {
    let userInput;
    do {
      userInput = prompt(message);
      if (!validationFunction(userInput)) {
        alert(errorMessage);
      }
    } while (!validationFunction(userInput));
    return userInput;
  }
  
  // Validation functions
  function isValidAmount(amount) {
    return !isNaN(amount) && parseFloat(amount) >= 1000;
  }
  
  function isValidNumberOfYears(years) {
    return !isNaN(years) && parseInt(years) >= 1;
  }
  
  function isValidPercentage(percentage) {
    return !isNaN(percentage) && parseFloat(percentage) <= 100;
  }
  
  // Prompt user for initial amount until valid input is provided
  const initialAmount = parseFloat(
    promptUntilValid(
      "Enter initial amount of money:",
      isValidAmount,
      "Invalid input for initial amount. Please enter a valid amount (at least 1000)."
    )
  );
  
  // Prompt user for number of years until valid input is provided
  const numberOfYears = parseInt(
    promptUntilValid(
      "Enter number of years:",
      isValidNumberOfYears,
      "Invalid input for number of years. Please enter a valid number (at least 1)."
    )
  );
  
  // Prompt user for percentage of a year until valid input is provided
  const percentageOfYear = parseFloat(
    promptUntilValid(
      "Enter percentage of a year:",
      isValidPercentage,
      "Invalid input for percentage of a year. Please enter a valid percentage (at most 100)."
    )
  );
  
  // Calculate total profit and total amount for each year
  let totalProfit = 0;
  let totalAmount = initialAmount;
  
  for (let year = 1; year <= numberOfYears; year++) {
    const yearlyProfit = (totalAmount * percentageOfYear) / 100;
    totalProfit += yearlyProfit;
    totalAmount += yearlyProfit;
  }
  
  // Display the final result
  alert(
    `Initial amount: ${initialAmount}\nNumber of years: ${numberOfYears}\nPercentage of year: ${percentageOfYear}\n\nTotal profit: ${totalProfit.toFixed(2)}\nTotal amount: ${totalAmount.toFixed(2)}`
  );
  
/*
// Prompt user for initial amount, number of years, and percentage of a year
let initialAmount = prompt('Enter initial amount of money:');
let numberOfYears = prompt('Enter number of years:');
let percentageOfYear = prompt('Enter percentage of a year:');

// Validate input data
if (
  isNaN(initialAmount) ||
  isNaN(numberOfYears) ||
  isNaN(percentageOfYear) ||
  initialAmount < 1000 ||
  numberOfYears < 1 ||
  percentageOfYear > 100
) {
  alert('Invalid input data');
} else {
  // Convert input values to numbers
  initialAmount = parseFloat(initialAmount);
  numberOfYears = parseInt(numberOfYears);
  percentageOfYear = parseFloat(percentageOfYear);

  // Initialize variables
  let totalProfit = 0;
  let totalAmount = initialAmount;

  // Calculate total profit and total amount for each year
  let year=1;
  for ( year = 1; year <= numberOfYears; year++) {
    const yearlyProfit = totalAmount * percentageOfYear / 100;
    totalProfit += yearlyProfit;
    totalAmount += yearlyProfit;
  }
  
    // Display final results
   alert(
    `Initial amount: ${initialAmount}\nNumber of years: ${numberOfYears}\nPercentage of year: ${percentageOfYear}\n\nTotal profit: ${totalProfit.toFixed(2)}\nTotal amount: ${totalAmount.toFixed(2)}`
  );
}
*/