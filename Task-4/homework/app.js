
//1
function reverseNumber(num) {
    const reversed = parseInt(num.toString().split('').reverse().join(''));
    return Math.sign(num) * reversed;
  }
  
  //2
  function forEach(arr, func) {
    for (let i = 0; i < arr.length; i++) {
      func(arr[i]);
    }
  }
  
  //3
  function map(arr, func) {
    const result = [];
    forEach(arr, function(el) {
      result.push(func(el));
    });
    return result;
  }
  
  //4
  function filter(arr, func) {
    const result = [];
    forEach(arr, function(el) {
      if (func(el)) {
        result.push(el);
      }
    });
    return result;
  }
  
  //5
  function getAdultAppleLovers(data) {
    return map(filter(data, function(person) {
      return person.age > 18 && person.favoriteFruit === 'apple';
    }), function(person) {
      return person.name;
    });
  }
  
  //6
  function getKeys(obj) {
    const keys = [];
    for (let key in obj) {
      if (obj.hasOwnProperty(key)) {
        keys.push(key);
      }
    }
    return keys;
  }
  
  //7
  function getValues(obj) {
    const values = [];
    for (let key in obj) {
      if (obj.hasOwnProperty(key)) {
        values.push(obj[key]);
      }
    }
    return values;
  }

// data for 5
  const data=[
    {
      "_id": "5b5e3168c6bf40f2c1235cd6",
      "index": 0,
      "age": 39,
      "eyeColor": "green",
      "name": "Stein",
      "favoriteFruit": "apple"
    },
    {
      "_id": "5b5e3168e328c0d72e4f27d8",
      "index": 1,
      "age": 38,
      "eyeColor": "blue",
      "name": "Cortez",
      "favoriteFruit": "strawberry"
    },
    {
      "_id": "5b5e3168cc79132b631c666a",
      "index": 2,
      "age": 2,
      "eyeColor": "blue",
      "name": "Suzette",
      "favoriteFruit": "apple"
    },
    {
      "_id": "5b5e31682093adcc6cd0dde5",
      "index": 3,
      "age": 17,
      "eyeColor": "green",
      "name": "Weiss",
      "favoriteFruit": "banana"
    }
  ]

//1
console.log(reverseNumber(12345)); // should return 54321
console.log(reverseNumber(-56789)); // should return -98765

//2
forEach([2, 5, 8], function(el) {
  console.log(el);
});

// 3
console.log(map([2, 5, 8], function(el) {
  return el + 3;
})); // should return [5, 8, 11]

console.log(map([1, 2, 3, 4, 5], function(el) {
  return el * 2;
})); // should return [2, 4, 6, 8, 10]

//4
console.log(filter([2, 5, 1, 3, 8, 6], function(el) {
  return el > 3;
})); // should return [5, 8, 6]

console.log(filter([1, 4, 6, 7, 8, 10], function(el) {
  return el % 2 === 0;
})); // should return [4, 6, 8, 10]

//5
console.log(getAdultAppleLovers(data)); // should return ['Stein']

//6
console.log(getKeys({ keyOne: 1, keyTwo: 2, keyThree: 3 })); // should return ["keyOne", "keyTwo", "keyThree"]

//7
console.log(getValues({ keyOne: 1, keyTwo: 2, keyThree: 3 })); // should return [1, 2, 3]
  